import View from './view';
import Fighter from './fighter';

class FighterView extends View {
  constructor(fighter: Fighter, handleClick, handleChecked) {
    super();
    
    this.createFighter(fighter, handleClick, handleChecked);
  }

  createFighter(fighter: Fighter, handleClick, handleChecked) {
    const { _id, name, source } = fighter;
    const nameElement = this.createName(name);
    const imageElement = this.createImage(source);
    const checkboxElement = this.createCheckbox(_id);

    this.element = this.createElement({ tagName: 'div', className: 'fighter' });
    this.element.append(imageElement, nameElement, checkboxElement);
    this.element.addEventListener('click', event => handleClick(event, fighter), false);
    checkboxElement.addEventListener('click', event => handleChecked(event, fighter, checkboxElement.checked), false);
  }

  createName(name: string): HTMLSpanElement {
    const nameElement = this.createElement({ tagName: 'span', className: 'name' });
    nameElement.innerText = name;

    return nameElement;
  }
  createCheckbox(id: number): HTMLInputElement {
      const attributes = { type: 'checkbox', id: `fighter${id}` };
      const checkboxElement = this.createElement({
        tagName: 'input',
        className: 'fighter-checkbox',
        attributes
      });
      return <HTMLInputElement>checkboxElement;
  }

  createImage(source: string): HTMLImageElement {
    const attributes = { src: source };
    const imgElement = this.createElement({
      tagName: 'img',
      className: 'fighter-image',
      attributes
    });
    return <HTMLImageElement>imgElement;
  }
}

export default FighterView;