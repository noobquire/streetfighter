import View from './view';
import FighterView from './fighterView';
import Fighter from './fighter';

class FightView extends View {
    element: HTMLElement;
    /**
     *
     */
    constructor(fighter1: Fighter, fighter2: Fighter) {
        super();
        this.element = this.createElement({ tagName: 'div', className: 'fight' });
        const fighter1Element = new FighterView(fighter1, null, null);
        const fighter2Element = new FighterView(fighter2, null, null);
        fighter2Element.element.style.transform = 'scaleX(-1)';

        this.fight(fighter1, fighter2);
    }

    fight(fighter1: Fighter, fighter2: Fighter): void {
        if (fighter1.health < 0 || fighter2.health < 0) {
            return;
        }
        const damage1 = fighter2.getHitPower() - fighter1.getBlockPower();
        const damage2 = fighter1.getHitPower() - fighter2.getBlockPower();
        fighter1.health -= damage1 > 0 ? damage1 : 0;
        fighter2.health -= damage2 > 0 ? damage2 : 0;
        setTimeout(() => this.fight(fighter1, fighter2), 1000);
    }


}

export default FightView;