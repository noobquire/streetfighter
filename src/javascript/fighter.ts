class Fighter {
    _id: number;
    name: string;
    health: number;
    attack: number;
    defense: number;
    source: string;

    getHitPower(): number {
        const criticalHitChance = Math.random() + 1;

        return this.attack * criticalHitChance;
    }

    getBlockPower(): number {
        const dodgeChance = Math.random() + 1;

        return this.defense * dodgeChance;
    }
}

export default Fighter;