import View from './view';
import FighterView from './fighterView';
import Fighter from './fighter';
import { fighterService } from './services/fightersService';
import FightView from './fightView';

class FightersView extends View {

  selectedFighters: Fighter[] = [];
  fightersDetailsMap: Map<number, Fighter> = new Map<number, Fighter>();

  constructor(fighters: Fighter[]) {
    super();
    
    this.createFighters(fighters);
    
    var modal = document.getElementById("fighterModal");
    var span = <HTMLSpanElement>document.getElementsByClassName("close")[0];
    span.onclick = function() {
      modal.style.display = "none";
    }
    window.onclick = function(event) {
      if (event.target == modal) {
        modal.style.display = "none";
      }
    }
  }

 

  createFighters(fighters: Fighter[]) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, this.handleFighterClick, this.handleFighterChecked);
      return fighterView.element;
    });

    this.element = this.createElement({ tagName: 'div', className: 'fighters' });
    this.element.append(...fighterElements);
  }

  async handleFighterClick(event: MouseEvent, fighter: Fighter) {
    var fighterDetails = await fighterService.getFighterDetails(fighter._id);
    console.log(fighterDetails);
    //this.fightersDetailsMap.set(fighter._id, fighter);
    var modal = document.getElementById("fighterModal");
    modal.style.display = "block";

    (<HTMLInputElement>document.getElementById('fighterName')).value = fighterDetails.name;
    (<HTMLInputElement>document.getElementById('fighterHealth')).value = fighterDetails.health.toString();
    (<HTMLInputElement>document.getElementById('fighterAttack')).value = fighterDetails.attack.toString();
    (<HTMLInputElement>document.getElementById('fighterDefense')).value = fighterDetails.defense.toString();
  }

  handleFighterChecked(event: MouseEvent, fighter: Fighter) {
    var fighterCheckbox = <HTMLInputElement>document.getElementById(`fighter${fighter._id}`);
    if(fighterCheckbox.checked) {
      this.selectedFighters.push(fighter);
      } else {
      const index = this.selectedFighters.indexOf(fighter, 0);
      if (index > -1) {
        this.selectedFighters.splice(index, 1);
      }
    }
  }

  handleStartButtonClick(event: MouseEvent, fighter: Fighter) {
    if(this.selectedFighters.length == 2) {
      var fight = new FightView(this.selectedFighters[0], this.selectedFighters[1]);
      const rootElement = document.getElementById('root');
      this.element.style.display = 'none';
      rootElement.appendChild(fight.element);
    } else {
      alert('select exactly 2 fighters');
    }
  }

  handleFormButtonClick(event: MouseEvent, fighter: Fighter) {
    var fighter: Fighter = this.fightersDetailsMap.get(fighter._id);
    const form = document.querySelector('form');
    const data = new FormData(form);

    fighter.name = (<HTMLInputElement>document.getElementById('fighterName')).value;
    fighter.health = +(<HTMLInputElement>document.getElementById('fighterHealth')).value;
    fighter.attack = +(<HTMLInputElement>document.getElementById('fighterAttack')).value;
    fighter.defense = +(<HTMLInputElement>document.getElementById('fighterDefense')).value;
  } 

}

export default FightersView;